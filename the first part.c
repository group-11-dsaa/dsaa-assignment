#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 51 // 50 characters + null terminator
#define DATE_OF_BIRTH_LENGTH 11 // YYYY-MM-DD + null terminator
#define REGISTRATION_NUMBER_LENGTH 7 // 6 digits + null terminator
#define PROGRAM_CODE_LENGTH 5 // 4 characters + null terminator

typedef struct {
    char name[MAX_NAME_LENGTH];
    char dateOfBirth[DATE_OF_BIRTH_LENGTH];
    char registrationNumber[REGISTRATION_NUMBER_LENGTH];
    char programCode[PROGRAM_CODE_LENGTH];
    float annualTuition;
} Student;

// Comparator function for qsort
int compareStudents(const void *a, const void *b) {
    const Student *studentA = (const Student *)a;
    const Student *studentB = (const Student *)b;

    // Compare based on name
    int result = strcmp(studentA->name, studentB->name);
    if (result != 0) {
        return result;
    }

    // If names are the same, compare based on registration number
    return strcmp(studentA->registrationNumber, studentB->registrationNumber);
}

// Function to sort students
void sortStudents(Student *students, int count, int sortBy) {
    if (sortBy == 1) {
        // Sort by name
        qsort(students, count, sizeof(Student), compareStudents);
    } else if (sortBy == 2) {
        // Sort by registration number
        // You can implement a separate comparator function for registration number if needed
        qsort(students, count, sizeof(Student), compareStudents);
    } else {
        printf("Invalid sort option.\n");
    }
}

void createStudent(Student *students, int *count) {
    if (*count >= 100) {
        printf("Maximum student limit reached.\n");
        return;
    }

    do {
        printf("Enter student name (50 characters or less): ");
        fflush(stdout); // Flush the output buffer to ensure prompt is displayed
        scanf("%50s", students[*count].name); // Read up to 50 characters, ignoring the rest
        if (strlen(students[*count].name) > 50) {
            printf("Error: Student name must be 50 characters or less. Please try again.\n");
            fflush(stdout); // Flush the output buffer to ensure prompt is displayed
        }
    } while (strlen(students[*count].name) > 50);

    do {
        printf("Enter date of birth (YYYY-MM-DD): ");
        fflush(stdout); // Flush the output buffer to ensure prompt is displayed
        scanf("%10s", students[*count].dateOfBirth); // Read up to 10 characters for date of birth
        if (strlen(students[*count].dateOfBirth) != 10) {
            printf("Error: Date of birth must be in the format YYYY-MM-DD. Please try again.\n");
            fflush(stdout); // Flush the output buffer to ensure prompt is displayed
        }
    } while (strlen(students[*count].dateOfBirth) != 10);

    do {
        printf("Enter registration number (6 digits or less): ");
        fflush(stdout); // Flush the output buffer to ensure prompt is displayed
        scanf("%7s", students[*count].registrationNumber); // Read exactly 6 characters for registration number
        if (strlen(students[*count].registrationNumber) > 6) {
            printf("Error: Registration number must be 6 digits or less. Please try again.\n");
            fflush(stdout); // Flush the output buffer to ensure prompt is displayed
        }
    } while (strlen(students[*count].registrationNumber) > 6);

    do {
        printf("Enter program code ( 4 characters or less): ");
        fflush(stdout); // Flush the output buffer to ensure prompt is displayed
        scanf("%5s", students[*count].programCode); // Read exactly 4 characters for program code
        if (strlen(students[*count].programCode) > 4) {
            printf("Error: Program code must be 4 or less characters. Please try again.\n");
            fflush(stdout); // Flush the output buffer to ensure prompt is displayed
        }
    } while (strlen(students[*count].programCode) > 4);

    // Validate annual tuition
    do {
        printf("Enter annual tuition (must be greater than 0): ");
        fflush(stdout); // Flush the output buffer to ensure prompt is displayed
        scanf("%f", &students[*count].annualTuition);
        if (students[*count].annualTuition <=0) {
            printf("Error: Annual tuition must be greater than 0. Please try again.\n");
            fflush(stdout); // Flush the output buffer to ensure prompt is displayed
        }
    } while (students[*count].annualTuition <= 0);

    (*count)++;
    printf("Student created successfully.\n");
}

void readStudent(Student *students, int count) {
    if (count == 0) {
        printf("No students to display.\n");
        return;
    }

    printf("Student Information:\n");
    for (int i = 0; i < count; i++) {
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].dateOfBirth);
        printf("Registration Number: %s\n", students[i].registrationNumber);
        printf("Program Code: %s\n", students[i].programCode);
        printf("Annual Tuition: %.2f\n", students[i].annualTuition);
        printf("\n");
    }
}

void updateStudent(Student *students, int count) {
    if (count == 0) {
        printf("No students to update.\n");
        return;
    }

    char regNum[REGISTRATION_NUMBER_LENGTH];
    printf("Enter registration number of student to update: ");
    scanf("%s", regNum);

    for (int i = 0; i < count; i++) {

        if (strcmp(students[i].registrationNumber, regNum) == 0) {

            do {
                printf("Enter student name (50 characters or less): ");
                fflush(stdout); // Flush the output buffer to ensure prompt is displayed
                scanf("%50s", students[i].name); // Read up to 50 characters, ignoring the rest
                if (strlen(students[i].name) > 50) {
                    printf("Error: Student name must be 50 characters or less. Please try again.\n");
                    fflush(stdout); // Flush the output buffer to ensure prompt is displayed
                }
            } while (strlen(students[i].name) > 50);

            do {
                printf("Enter date of birth (YYYY-MM-DD): ");
                fflush(stdout); // Flush the output buffer to ensure prompt is displayed
                scanf("%10s", students[i].dateOfBirth); // Read up to 10 characters for date of birth of student
                if (strlen(students[i].dateOfBirth) != 10) {
                    printf("Error: Date of birth must be in the format YYYY-MM-DD. Please try again.\n");
                    fflush(stdout); // Flush the output buffer to ensure prompt is displayed
                }
            } while (strlen(students[i].dateOfBirth) != 10);


            do {
                printf("Enter program code ( 4 characters or less): ");
                fflush(stdout); // Flush the output buffer to ensure prompt is displayed
                scanf("%5s", students[i].programCode); // Read exactly 4 characters for program code
                if (strlen(students[i].programCode) > 4) {
                    printf("Error: Program code must be 4 or less characters. Please try again.\n");
                    fflush(stdout); // Flush the output buffer to ensure prompt is displayed
                }
            } while (strlen(students[i].programCode) > 4);

            // Validate annual tuition
            do {
                printf("Enter annual tuition (must be greater than 0): ");
                fflush(stdout); // Flush the output buffer to ensure prompt is displayed
                scanf("%f", &students[i].annualTuition);
                if (students[i].annualTuition <=0) {
                    printf("Error: Annual tuition must be greater than 0. Please try again.\n");
                    fflush(stdout); // Flush the output buffer to ensure prompt is displayed
                }
            } while (students[i].annualTuition <= 0);


            printf("Student updated successfully.\n");
            return;
        }
    }
    printf("Student with registration number %s not found.\n", regNum);
}

void deleteStudent(Student *students, int *count) {
    if (*count == 0) {
        printf("No students to delete.\n");
        return;
    }

    char regNum[REGISTRATION_NUMBER_LENGTH];
    printf("Enter registration number of student to delete: ");
    scanf("%s", regNum);

    for (int i = 0; i < *count; i++) {
        if (strcmp(students[i].registrationNumber, regNum) == 0) {
            for (int j = i; j < *count - 1; j++) {
                strcpy(students[j].name, students[j + 1].name);
                strcpy(students[j].dateOfBirth, students[j + 1].dateOfBirth);
                strcpy(students[j].registrationNumber, students[j + 1].registrationNumber);
                strcpy(students[j].programCode, students[j + 1].programCode);
                students[j].annualTuition = students[j + 1].annualTuition;
            }
            (*count)--;
            printf("Student deleted successfully.\n");
            return;
        }
    }
    printf("Student with registration number %s not found.\n", regNum);
}


void searchByRegistrationNumber(Student *students, int count){
    if (count==0){
        printf("No student to search.\n");
        return;
    }
    char regNum[REGISTRATION_NUMBER_LENGTH];
    printf("Enter registration number of the student to search: ");
    scanf("%s",regNum);

    for(int i=0; i<count;i++){
        if (strcmp(students[i].registrationNumber,regNum)==0){
            printf("Student found:\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].dateOfBirth);
            printf("Registration Number: %s\n", students[i].registrationNumber);
            printf("Program Code: %s\n", students[i].programCode);
            printf("Annual Tuition: %.2f\n", students[i].annualTuition);
            return;
        }
    }
    printf("Student with registration number %s not found.\n", regNum);
}

void exportToCSV(Student *students, int count) {
    // Open the CSV file in append mode
    FILE *file = fopen("student_records.csv", "a");
    if (file == NULL) {
        printf("Error: Unable to open file for writing.\n");
        return;
    }

    // Write titles for each property
    fprintf(file, "Name,Date of Birth,Registration Number,Program Code,Annual Tuition\n");

    // Write student records to the file
    for (int i = 0; i < count; i++) {
        fprintf(file, "%s,%s,%s,%s,%.2f\n",
                students[i].name,
                students[i].dateOfBirth,
                students[i].registrationNumber,
                students[i].programCode,
                students[i].annualTuition);
    }

    fclose(file);
    printf("Student records exported to student_records.csv\n");
}

int main() {
    Student students[100]; // Assuming a maximum of 100 students
    int studentCount = 0;

    char choice[10]; // Change choice to a string
    int choiceInt; // Declare choiceInt outside the loop

    do {
        // Display menu options
        printf("\nMenu:\n");
        printf("1. Create Student\n");
        printf("2. Read Student\n");
        printf("3. Update Student\n");
        printf("4. Delete Student\n");
        printf("5. Search by Registration Number\n");
        printf("6. Sort Students\n");
        printf("7. Export Student Records to CSV\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%s", choice); // Read input as string

        // Convert choice to integer
        choiceInt = atoi(choice);

        // Perform corresponding action based on user's choice
        switch(choiceInt) {
            case 1:
                createStudent(students, &studentCount);
                break;
            case 2:
                readStudent(students, studentCount);
                break;
            case 3:
                updateStudent(students, studentCount);
                break;
            case 4:
                deleteStudent(students, &studentCount);
                break;
            case 5:
                searchByRegistrationNumber(students,studentCount);
                break;
            case 6: {
                // Submenu for sorting
                int sortBy;
                printf("\nSort Menu:\n");
                printf("1. Sort by Name\n");
                printf("2. Sort by Registration Number\n");
                printf("Enter your choice: ");
                scanf("%d", &sortBy);

                // Perform sorting
                sortStudents(students, studentCount, sortBy);

                // Print sorted list
                readStudent(students, studentCount);
                break;
            }
            case 7:
                exportToCSV(students, studentCount);
                break;
            case 8:
                printf("Exiting program.\n");
                break;
            default:
                printf("Invalid choice. Please try again.\n");
                break; // Added default case to handle invalid choices
        }
    } while(choiceInt != 8);

    return 0;
}
